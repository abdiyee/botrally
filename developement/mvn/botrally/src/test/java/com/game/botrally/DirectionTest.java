package com.game.botrally;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.game.botrally.model.Board;

import com.game.botrally.model.*;

import org.junit.Before;
import org.junit.Test;

public class DirectionTest 
{
    @Test
    public void testChangeDirection()
    {
        EmptyItem  item = new EmptyItem(new Cell(3,3),Direction.NORTH);
   
       assertEquals(Direction.WEST,Direction.NORTH.determineDirection(Action.TURNLEFT,item).getDirection());
       assertEquals(Direction.WEST,Direction.SOUTH.determineDirection(Action.TURNRIGHT,item).getDirection());
       assertEquals(Direction.EAST,Direction.WEST.determineDirection(Action.UTURN,item).getDirection());
    }
}