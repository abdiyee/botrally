package com.game.botrally;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import com.game.botrally.model.*;
import com.game.botrally.Game;

import org.junit.Before;
import org.junit.Test;

/***
 * MOOOOOVEEE TTTESSTSS
 */
public class GameTest {
    Game game;

    @Before
    public void initialize() {

        // seed is 23
        game = new Game(new String[] { "Abdi", "pen", "ed" }, 3, 3, 4);

    }

    /**
     * game.getBoard()
     */
    @Test
    public void testBoardActivation() {
        // place robot on conveyer
        game.getPlayers().get(2).robot.addAction(Action.FORWARD);
        game.getPlayers().get(2).robot.executeNext();

        Cell newLocation = game.getBoard().activateAll(game.getPlayers().get(2).robot).getCell();
        game.getPlayers().get(2).robot.setCell(newLocation);

        Cell expected = new Cell(9, 2);
        assert (expected.isEquals(game.getPlayers().get(2).robot.getCell()));

    }

    @Test
    public void testShiftOrder() {
        game.shiftExecutionOrder();

        int[] expectedOrder = new int[]{1,2,0};
        
        assertEquals(game.getOrder(), expectedOrder);
    }

    /**
     * game.getBoard()
     */

    /**
     * robot
     */

    @Test
    public void testInstruction() {

        // place robot on conveyer
        game.getPlayers().get(0).robot.addAction(Action.FORWARD);
        game.getPlayers().get(0).robot.executeNext();

        // should become 5 1
        assertTrue(game.getPlayers().get(0).getRobot().getCell().isEquals(new Cell(5, 1)));

    }

    @Test
    public void testRobotConstruction() {
        // robot should be made and placed in the bottom centetr
        assertTrue(game.getPlayers().get(0).getRobot().getCell().isEquals(new Cell(5, 0)));
        assertTrue(game.getPlayers().get(1).getRobot().getCell().isEquals(new Cell(6, 0)));
        assertTrue(game.getPlayers().get(2).getRobot().getCell().isEquals(new Cell(7, 0)));
    }

    @Test
    public void testProgram() {
        // robot should be made and placed in the bottom centetr
        Program newProgram = new Program();
        assertEquals(true, newProgram.add(Action.FORWARD));
        assertEquals(false, newProgram.add(Action.FORWARD));
        assertEquals(true, newProgram.add(Action.BACKWARD));
    }

    /**
     * robot
     */

    @Test
    public void checkNothingOverlaps() {
        boolean overlaps = false;
        for (int i = 0; i < game.getBoard().getItems().size(); i++) {
            for (int j = 0; j < game.getBoard().getItems().size(); j++) {
                if (i != j && game.getBoard().getItems().get(i).intersects(game.getBoard().getItems().get(j))) {
                    overlaps = true;
                }
            }
        }
        assertEquals(false, overlaps);
    }

    @Test
    public void testIntersect() {
        EmptyItem A = new EmptyItem(new Cell(1, 1), Direction.EAST);
        EmptyItem B = new EmptyItem(new Cell(1, 1), Direction.WEST);
        assertEquals(true, A.intersects(B));
    }

    @Test
    public void testPit() {
        EmptyItem deadItem = new EmptyItem(1);
        deadItem = (EmptyItem) game.getBoard().getItems().get(3).activate(deadItem);
        assertEquals(true, deadItem.getCell().outOfBounds());
    }

}