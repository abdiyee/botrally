package com.game.botrally.model;

import java.util.ArrayList;
import java.util.Stack;

public class Program // hidden from other packages as it is only required by board
{
    private ArrayList<Action> actions;
    public Program() {
        actions = new ArrayList<Action>();
    }

    public Action getNext() {
        
        Action nextActoin=actions.get(0);
        actions.remove(0);
        return nextActoin;
    }

    public boolean add(Action action) {
        
        // don't check for repetition if its the first action
        if(actions.size() == 0)
        {
            actions.add(action);
            return true;
        }
        else if (actions.get(actions.size()-1) != action) {
            actions.add(action);
            return true;
        } 
        else {
            return false;
        }

    }
}