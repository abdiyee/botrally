package com.game.botrally.model;

import java.util.ArrayList;
import java.util.Random;

import javax.annotation.processing.Generated;

public class Board {
    public static final Random rnd = new Random(345); // !!DONT CHANGE SEED AS TESTS WILL FAIL

    private ArrayList<Item> items;
    private int totalConveyerSize;
    private int totalPitSize;
    private int numOfConveyers;
    private int numOfPits;
    private int numOfFlags;

    public Board(int numOfConveyers, int numOfPits, int numOfFlags) {
        // !!DONT CHANGE SEED reset the this random with every new number
        totalConveyerSize = 0;
        totalPitSize = 0;
        this.numOfPits = numOfPits;
        this.numOfConveyers = numOfConveyers;
        this.numOfFlags = numOfFlags;
        // conveyers = new ArrayList<ConveyerLocation>();
        // pits = new ArrayList<PitLocation>();
        items = new ArrayList<Item>();
        GenerateItems();
        setFlags();

    }

    public void GenerateItems() {
        // generate conveyers
        for (int i = 0; i < numOfConveyers; i++) {
            items.add(new ConveyerSet());
            totalConveyerSize = totalConveyerSize + items.get(i).getSize();
        }
        // generate pits and ensure they dont overlap with conveyers
        for (int i = numOfConveyers; i < (numOfPits + numOfConveyers);) {
            PitLocation pit = new PitLocation();
            if (!isClashing(pit)) {
                this.items.add(pit);
                totalPitSize = totalPitSize + items.get(i).getSize();
                i++;
            }
        }
        // generate empty items for the remainder TOTAL - OCCUPIEDLOCATIONS
        int numOfRemainingLocations = (Cell.MAX_X * Cell.MAX_Y) - (totalConveyerSize + totalPitSize);

        for (int i = (totalPitSize + totalConveyerSize); i < numOfRemainingLocations;) {
            EmptyItem empty = new EmptyItem(true);
            if (!isClashing(empty)) {
                this.items.add(empty);
                i++;
            }
        }
    }

    public void setFlags() {
        // select generate locations for flags
        for (int i = 0; i < numOfFlags; i++) {
            // select an empty location by skipping conveyers and pits
            int selectedLocation = Board.rnd.nextInt(items.size()) + (totalPitSize + totalConveyerSize) - 2;
            items.get(selectedLocation).setFlag(true);
        }
    }

    public boolean collectFlag(Item comp) // only if the item intersects with
    {
        for (Item item : items) {
            if (item.intersects(comp) && item.getFlag()) {
                item.setFlag(false);
                return true;
            }
        }
        return false;
    }

    // activate all
    public Item activateAll(Item comp) // only if the item intersects with
    {
        for (Item item : items) {
            if (comp.intersects(item)) {
                return item.activate(comp);
            }
        }
        return comp;
    }

    // Activate a location
    public ArrayList<Item> getItems() {
        return this.items;
    }

    // check if two items clash with any of the items currently on the system
    public boolean isClashing(Item givenLocation) {
        for (Item location : items) {

            if (location.intersects(givenLocation)) {
                return true;
            }

        }
        return false;
    }


    public int getTotalConveyerSize() {
        return this.totalConveyerSize;
    }

    public int getTotalPitSize() {
        return this.totalPitSize;
    }

    public int getNumOfConveyers() {
        return this.numOfConveyers;
    }

    public int getNumOfPits() {
        return this.numOfPits;
    }

    public int getNumOfFlags() {
        return this.numOfFlags;
    }

}