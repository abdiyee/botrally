package com.game.botrally.model;

public class Robot extends Item {

    private Program program;

    public Robot(int StatingX) {

        super(new Cell(5 + StatingX, 0), Direction.NORTH);
        program = new Program();
    }

    @Override // push
    public Item activate(Item position) {
        return Action.FORWARD.act(this);
    }
    public boolean addAction(Action action)
    {
        return program.add(action);
    }
    public void executeNext() {
        Item result = program.getNext().act(this);
        this.setCell(result.getCell());
        this.changeDirection(result.getDirection());
    }

}