package com.game.botrally.model;

import java.util.ArrayList;

class ConveyerSet extends Item // hidden from other packages as it is only required by board
{

    private ArrayList<EmptyItem> tail;
    private EmptyItem head;

    public ConveyerSet() {
        super(3);
        // now set the cells
        tail = new ArrayList<EmptyItem>();
     

        // create starting position
        tail.add(new EmptyItem(this.getCell(),this.getDirection()));
        generateStraight(Cell.MAX_X - 8);

        EmptyItem lastTail = tail.get(tail.size() - 1);
        if (Board.rnd.nextBoolean()) {
            head = (EmptyItem)lastTail.getDirection().determineDirection(Action.TURNLEFT, lastTail);
        } else {
            head = (EmptyItem)Action.FORWARD.act(lastTail.getDirection().determineDirection(Action.TURNRIGHT, lastTail));
        }

    }

    @Override
    public int getSize() {
        return (tail.size() + 1); // one for the head
    }

    @Override
    public boolean intersects(Item location) {
        /*
         * check if it clashes with tail if not check if clashes with head
         */

        for (Item var : tail) {
            if (var.getCell().isEquals(location.getCell())) {
                return true;
            }
        }

        return head.getCell().isEquals(location.getCell());

    }

    public static int getTotal(ArrayList<ConveyerSet> conveyers) {
        int total = 0;
        for (ConveyerSet conveyer : conveyers) {
            total = total + conveyer.getSize();
        }
        return total;
    }

    private void generateStraight(int maxLength) {
        int tailSize = Board.rnd.nextInt(maxLength);

        for (int i = 1; i < tailSize; i++) {
            EmptyItem newItem = new EmptyItem(1);
            EmptyItem result = (EmptyItem) Action.FORWARD.act(tail.get(i - 1));
            newItem.setCell(new Cell(result.getCell().getX(), result.getCell().getY()));

            newItem.changeDirection(this.getDirection());
            tail.add(newItem);

        }
    }

    @Override // does nothing
    public Item activate(Item item) {

     //   for (Item var : tail) {
     //       if (var.getCell().isEquals(item.getCell())) {
                return Action.FORWARD.act(head);
      //      }
      //  }
    //    return item;
        /*
         * // if intersect move bot from location to location for (int i = 0; i <
         * tail.size(); i++) { if (item.getCell().isEquals(tail.get(i).getCell())) {
         * item = Action.FORWARD.act(tail.get(i)); } } if
         * (item.getCell().isEquals(head.getCell())) { item = Action.FORWARD.act(head);
         * } return item; // because this location does nothing
         */
    }

}