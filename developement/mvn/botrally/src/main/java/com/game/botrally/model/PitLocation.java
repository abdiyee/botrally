package com.game.botrally.model;

class PitLocation extends Item {
  
    public PitLocation() {
        super(0);


    }
  

    @Override
    public Item activate(Item position) {
        position.setCell(new Cell(-1,-1));
        return position;
    }

    @Override
    public int getSize() {
        return 1;
    }

 
   

  

}