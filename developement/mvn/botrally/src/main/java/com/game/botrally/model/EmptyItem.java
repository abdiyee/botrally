package com.game.botrally.model;

public class EmptyItem extends Item  // hidden from other packages as it is only required by board
{
   
    public static Cell nextAvailable = new Cell(0,0);;
  
    public EmptyItem(Boolean getNext)
    {
        super(0);
        if (getNext) {
            this.setCell(
                new Cell(
                nextAvailable.getX(), nextAvailable.getY())
            );
            
            
            // Please next cell
           // this.setCell(EmptyLocation.nextAvailable);
    
            // 
            if (EmptyItem.nextAvailable.getX() == Cell.MAX_X) {
                EmptyItem.nextAvailable.setY(EmptyItem.nextAvailable.getY()+1);
                EmptyItem.nextAvailable.setX(0);
            }
            else
            {
             
                EmptyItem.nextAvailable.setX(EmptyItem.nextAvailable.getX()+1);
            }
        }
        else
        {
            this.setCell(
                new Cell(
                0, 0)
            );
        }
                  
    }
    public EmptyItem(int margin)
    {
        super(margin);
    }
    public EmptyItem(Cell cell,Direction direction)
    {
        super(cell, direction);
    }
    @Override // does nothing
    public Item activate(Item position) {
        return position; // because this location does nothing
    }

    @Override
    public int getSize() {
        return 0;
    }

  


}