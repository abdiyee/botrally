package com.game.botrally.model;

public class Player 
{
    public static int numOfPlayers = 0;
    private String name;
    private int flags;
    public Robot robot;
    private int code;

    public Player(String name) {
       
        this.name = name;
        this.flags = 0;
        this.robot = new Robot(numOfPlayers); // place on x based on player number
        
        this.code = Player.numOfPlayers;
        Player.numOfPlayers++;
    }

    public String getName() {
        return this.name;
    }

    public int getFlags() {
        return this.flags;
    }

    public void addFlags(int flags) {
        this.flags = this.flags + flags;
    }

    public Robot getRobot() {
        return this.robot;
    }

    public int getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        return "{" +
            "name   = '" + name + "'" +
            "flags = '" + flags + "'" +
            "robot  = '" + robot + "'" +
            "code   = '" + code + "'" +
            "}";
    }





}