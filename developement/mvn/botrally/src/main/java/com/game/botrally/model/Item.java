package com.game.botrally.model;

public abstract class Item {
    private Cell cell;
    private Direction direction;
    private boolean flag;

    public Item(Cell cell, Direction direction) {
        this.flag = false;
        this.cell = cell;
        this.direction = direction;
    }

    public boolean getFlag() {
        return flag;
    }

    public boolean setFlag(boolean flag) {
        return this.flag = flag;
    }

    public Item(Cell cell) {
        this.cell = cell;
        direction = Direction.getRnd();
    }

    public Item(int margin) {

        this.cell = new Cell(Board.rnd.nextInt(Cell.MAX_X - margin), Board.rnd.nextInt(Cell.MAX_Y - margin));
        direction = Direction.getRnd();
    }

    public void move(int x, int y) {
        this.cell.setX(this.cell.getX() + x);
        this.cell.setY(this.cell.getY() + y);

    }

    public Item changeDirection(Direction direction) {
        this.direction = direction;
        return this;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public Cell getCell() {
        return this.cell;
    }

    public boolean isEquals(Item item) {
        return (item.cell.isEquals(this.cell)) && (item.direction == this.direction);
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    abstract public Item activate(Item position);

    public int getSize() {
        return 1;
    };

    public boolean intersects(Item location) {
        return this.getCell().isEquals(location.getCell());
    }

}