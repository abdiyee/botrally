package com.game.botrally.model;
public class Cell{

    public static final int MAX_X = 12;
    public static final int MAX_Y = 7;
    private int x;
    private int y;


    public Cell(int x, int y) {
        if (outOfBounds()) {
            throw new IllegalArgumentException();
        }
        this.x = x;
        this.y = y;
    }
    // A pushes B is equivelant to A's cell if A moved forward and B's direction

    public boolean isEquals(Cell comp)
    {
        return (comp.x == x &&  comp.y == y);
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public boolean outOfBounds()
    {
        return (x > Cell.MAX_X || x < 0 ) || (y > Cell.MAX_Y || y < 0 );
    }

    @Override
    public String toString() {
        return  " ("+getX()+","+getY()+") ";
    }

}