package com.game.botrally.testing;

import com.game.botrally.*;
import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.print.DocFlavor.URL;

import com.game.botrally.model.*;

import javafx.stage.FileChooser;

import com.game.botrally.Game;

import org.junit.Before;
import org.junit.Test;

/***
 * MOOOOOVEEE TTTESSTSS
 */
public class GameTest {

	char[][] board;

	Game game;
	Game gameFiles;

	@Before
	public void initialize() {

		// seed is 23

		fillArray();
		game = new Game(new String[] { "Abdi", "pen", "ed" },new Cell(5,0), 3, 3, 4);
		gameFiles = new Game(board, new String[] { "Abdi", "pen", "ed" });
			}

	/**
	 * game.getBoard()
	 */
	@Test
	public void testBoardActivation() {
		// place robot on conveyer
		game.getPlayers().get(2).robot.addAction(Action.FORWARD);
		game.getPlayers().get(2).robot.executeNext();

		Cell newLocation = game.getBoard().activateAll(game.getPlayers().get(2).robot).getCell();
		game.getPlayers().get(2).robot.setCell(newLocation);

		// robot falls into pit after board activation
		assert(game.getPlayers().get(2).robot.getCell().outOfBounds());

	}

	@Test
	public void testShiftOrder() {
		game.shiftExecutionOrder();

		int[] expectedOrder = new int[] { 1, 2, 0 };

		assertEquals(game.getOrder(), expectedOrder);
	}

	/**
	 * game.getBoard()
	 */

	/**
	 * robot
	 */

	@Test
	public void testInstruction() {

		// place robot on conveyer
		game.getPlayers().get(0).robot.addAction(Action.FORWARD);
		game.getPlayers().get(0).robot.executeNext();

		// should become 5 1
		assertTrue(game.getPlayers().get(0).getRobot().getCell().isEquals(new Cell(5, 1)));

	}

	@Test
	public void testRobotConstruction() {
		// robot should be made and placed in the bottom centetr
		assertTrue(game.getPlayers().get(0).getRobot().getCell().isEquals(new Cell(5, 0)));
		assertTrue(game.getPlayers().get(1).getRobot().getCell().isEquals(new Cell(6, 0)));
		assertTrue(game.getPlayers().get(2).getRobot().getCell().isEquals(new Cell(7, 0)));
	}

	@Test
	public void testProgram() {
		// robot should be made and placed in the bottom center
		Program newProgram = new Program();
		assertEquals(true, newProgram.add(Action.FORWARD));
		assertEquals(false, newProgram.add(Action.FORWARD));
		assertEquals(true, newProgram.add(Action.BACKWARD));
	}

	/**
	 * robot
	 */

	@Test
	public void checkNothingOverlaps() {
		boolean overlaps = false;
		for (int i = 0; i < game.getBoard().getItems().size(); i++) {
			for (int j = 0; j < game.getBoard().getItems().size(); j++) {
				if (i != j && game.getBoard().getItems().get(i).intersects(game.getBoard().getItems().get(j))) {
					overlaps = true;
				}
			}
		}
		assertEquals(false, overlaps);
	}

	@Test
	public void testIntersect() {
		EmptyItem A = new EmptyItem(new Cell(1, 1), Direction.EAST);
		EmptyItem B = new EmptyItem(new Cell(1, 1), Direction.WEST);
		assertEquals(true, A.intersects(B));
	}

	@Test
	public void testPit() {
		EmptyItem deadItem = new EmptyItem(1);
		deadItem = (EmptyItem) game.getBoard().getItems().get(3).activate(deadItem);
		assertEquals(true, deadItem.getCell().outOfBounds());
	}

	/************************************ testing *************************************************/
	public void fillArray() {

		ArrayList<String> lineContent = new ArrayList<String>();
		
		BufferedReader reader = null;
		java.net.URL url = this.getClass().getResource( "spec-board.brd" );
		File file = new File(url.getPath());
		
		try {
			FileReader boardFile = new FileReader(file);
			reader = new BufferedReader(boardFile);
			String line = reader.readLine();

			if (line.contentEquals("format 1")) {
				System.out.println("The format (format 1) is Correct!");
			} else {
				System.out.println("The format is incorrect! File must start with format 1");
			}

			line = reader.readLine();
			while (line != null) {

				lineContent.add(line);
				line = reader.readLine();

			}

			if (lineContent.size() < 1) {
				System.out.println("There is nothing on the Board!");
				reader.close();
				return;

			}

			int ysize = lineContent.size();
			int xsize = lineContent.get(0).length();

			board = new char[ysize][xsize];
			String tmpline = lineContent.get(0);

			for (String s : lineContent) {

				if (s.length() == tmpline.length()) {

					tmpline = s;
				} else {

					System.out.println("The Lengths of the rows are not the same!");
					reader.close();
					return;
				}

			}

			for (int i = 0; i < lineContent.size(); i++) {
				String s = lineContent.get(i);
				int count = 0;
				char[] a = s.toCharArray();
				for (char c : a) {
					board[i][count] = c;
					System.out.print(board[i][count]);
					count++;
				}
				System.out.println();
			}

			reader.close();
		} catch (IOException e) {
			System.out.println("The File Does not Exist!");
		}

	}
	/************************************ testing *************************************************/

}