package com.game.botrally.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.game.botrally.model.*;

import org.junit.Before;
import org.junit.Test;

public class ActionTest {
    public Item item;
    @Before
    public void initialize()
    {
        item = new EmptyItem(new Cell(3,3),Direction.NORTH);
    }
    @Test
    public void testForward() {
        EmptyItem expectedItem = new EmptyItem(new Cell(3,4),Direction.NORTH);
      
        Item result = Action.FORWARD.act(item);

        assert(result.isEquals(expectedItem));
     
    }
    @Test
    public void testBackward() {
        EmptyItem expectedItem = new EmptyItem(new Cell(3,2), Direction.NORTH);

        Item result = Action.BACKWARD.act(item);

        assert(result.isEquals(expectedItem));
    }
    @Test
     public void testTurnLeft() {
        EmptyItem expectedItem = new EmptyItem(new Cell(3,3), Direction.WEST);

   
        Item result = Action.TURNLEFT.act(item);

        assert(result.isEquals(expectedItem));
    }
    @Test
    public void testUTurn() {
        
        Cell expectedCell  = new Cell(3,3);
        Direction expectedDirect  = Direction.SOUTH;

        Item result = Action.UTURN.act(item);

        assert(expectedCell.isEquals(result.getCell()));
        assertEquals(expectedDirect, result.getDirection());
    }

    @Test
    public void testTurnRight() {
        
        Cell expectedCell  = new Cell(3,3);
        Direction expectedDirect  = Direction.EAST;

        Item result = Action.TURNRIGHT.act(item);

        assert(expectedCell.isEquals(result.getCell()));
        assertEquals(expectedDirect, result.getDirection());
    }
}