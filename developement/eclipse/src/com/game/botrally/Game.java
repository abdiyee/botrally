
package com.game.botrally;

import java.util.ArrayList;
import com.game.botrally.model.Player;
import com.game.botrally.model.Cell;
import com.game.botrally.model.Board;
import com.game.botrally.model.Action;

public class Game {
	private ArrayList<Player> players;
	private Board board;
	private int[] order;
	private char[][] charBoard;

	public Game(String[] names,Cell lineStart, int numOfConveyers, int numOfPits, int numOfFlags) {
		board = new Board(numOfConveyers, numOfPits, numOfFlags);
		players = new ArrayList<Player>();
		order = new int[names.length];
		
		players.add(new Player(names[0],lineStart));
		for (int i = 1; i < names.length; i++) {
			// get previous location + 1 to x
			Cell prev = players.get(i-1).robot.getCell();
			Cell nextCell = new Cell(prev.getX()+1,0);
			
			players.add(new Player(names[i], nextCell));
			order[i] = i;
		}

		//this.start();
	}
	
	public Game(char[][] charBoard, String[] names) {
		this.charBoard = charBoard;
		board = new Board(charBoard);

		players = new ArrayList<Player>();
		order = new int[names.length];

		for (int i = 0; i < names.length; i++) {
			players.add(new Player(names[i],new Cell(0,1)/*asdasd*/));
			order[i] = i;
		}
		printBoard();
	}
	
	public void printBoard()
	{
		for (int i = 0; i < charBoard.length; i++) {
		
			for (int j = 0; j < charBoard[i].length; j++) {
			
				System.out.print(charBoard[i][j]);
			
			}
			System.out.println();
		}
	}
	public void start() {
		boolean play = true;
		while (play) {
			programRobots();
			// create the exection order for the robots
			shiftExecutionOrder();
			for (int i = 0; i < players.size(); i++) {
				// execute instruction
				players.get(order[i]).robot.executeNext();

				// activate
				players.get(order[i]).robot.setCell(board.activateAll(players.get(order[i]).robot).getCell());

				// if the players robot
				if (players.get(i).robot.getCell().outOfBounds()) {
					resetPlayer(i);
			
				}

				// try collecting flags
				if (board.collectFlag(players.get(i).robot)) {
					players.get(i).addFlags(1);
				}

				// if theres no more flags or only one player remaining end game
				if (totalPoints() >= board.getNumOfFlags()) {
					int winner = getWinning(); // Use this index to display
				// right player
					play = false;
					break;
				}
				
			}
		}
		// game over
	}

	public int totalPoints() {
		int total = 0;
		for (Player player : players) {
			total = total + player.getFlags();
		}
		return total;
	}

	public int getWinning() {
		int winning = 0;
		for (int i = 0; i < players.size(); i++) {
			if (players.get(winning).getFlags() > players.get(i).getFlags()) {
				winning = i;
			}
		}
		return winning;
	}

	public void resetPlayer(int index) {
		players.get(index).robot.resetToStart();
		/*
		 * int[] newOrder = new int[order.length - 2]; // remove the player to
		 * remove and the 1 becuase .size get // maxindex+1 for (int i = 0; i <
		 * order.length - 1;) { if (order[i] != index) { newOrder[i] = order[i];
		 * i++; } } order = newOrder;
		 */
		// remove player visually
	}

	public void shiftExecutionOrder() {
		int first = order[0];
		for (int i = 1; i <= order.length; i++) {
			order[i - 1] = i;
		}
		order[order.length - 1] = first;
	}

	public void programRobots() {
		for (Player player : players) {

			for (int i = 0; i < 5; i++) { // add 5 actions
				// ask via ui. But now just select random
				Action inputAction = Action.getRnd();
				while (!player.robot.addAction(inputAction)) {
					inputAction = Action.getRnd();
				}

			}

		}
	}

	public ArrayList<Player> getPlayers() {
		return this.players;
	}

	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}

	public Board getBoard() {
		return this.board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public int[] getOrder() {
		return this.order;
	}

	public void setOrder(int[] order) {
		this.order = order;
	}
}