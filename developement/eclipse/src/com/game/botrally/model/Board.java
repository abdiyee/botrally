package com.game.botrally.model;

import java.util.ArrayList;
import java.util.Random;

public class Board {
	public static final Random rnd = new Random(345); // !!DONT CHANGE SEED AS
														// TESTS WILL FAIL

	private ArrayList<Item> items;
	private int totalConveyerSize;
	private int totalPitSize;
	private int numOfConveyers;
	private int numOfPits;
	private int numOfFlags;
	private int numOfGears;
	private int lastFlag;
	public Board(char[][] charBoard) {
		lastFlag=0;
		this.numOfPits = 0;
		this.numOfConveyers = 0;
		this.numOfFlags = 0;
		totalConveyerSize = 0;
		totalPitSize = 0;
		numOfGears = 0;
		items = new ArrayList<Item>();
		// create graph using graph
		for (int i = 0; i < charBoard.length; i++) {
			for (int j = 0; j < charBoard[i].length; j++) {
				switch (charBoard[i][j]) {
				case '.':
					items.add(new EmptyItem(new Cell(i, j)));
					break;
				case '>':// east conveyers
					ConveyerSet covEast = new ConveyerSet(new EmptyItem(new Cell(i, j), Direction.EAST)); 
					while (j + 1 < charBoard[i].length && charBoard[i][j + 1] == '>') {
						j++;
						covEast.setHead(new EmptyItem(new Cell(i, j), Direction.EAST));
						covEast.addToTail(new EmptyItem(new Cell(i, j), Direction.EAST));
					}
					items.add(covEast);
					numOfConveyers++;
					break;
				case '<':
					ConveyerSet covSouth = new ConveyerSet(new EmptyItem(new Cell(i, j), Direction.SOUTH));
					while (j + 1 < charBoard[i].length && charBoard[i][j + 1] == '<') {
						j++;
						covSouth.addToTail(new EmptyItem(new Cell(i, j), Direction.SOUTH));
					}
					items.add(covSouth);
					numOfConveyers++;
					break;
				case 's':
				case 'S': // if it's a corner conveyer
					ConveyerSet cornerCovSouth = new ConveyerSet(new EmptyItem(new Cell(i, j), Direction.SOUTH));
					items.add(cornerCovSouth);
					numOfConveyers++;
					break;
				case 'e':
				case 'E': // if it's a corner conveyer
					ConveyerSet cornerCovEast = new ConveyerSet(new EmptyItem(new Cell(i, j), Direction.EAST));
					items.add(cornerCovEast);
					numOfConveyers++;
					break;
				case 'w':
				case 'W': // if it's a corner conveyer
					ConveyerSet cornerCovWest = new ConveyerSet(new EmptyItem(new Cell(i, j), Direction.WEST));
					items.add(cornerCovWest);
					numOfConveyers++;
					break;
				case 'n':
				case 'N': // if it's a corner conveyer
					ConveyerSet cornerCovNorth = new ConveyerSet(new EmptyItem(new Cell(i, j), Direction.NORTH));
					items.add(cornerCovNorth);
					numOfConveyers++;
					break;
				case '^': // on conveyer facing up
					items.add(new ConveyerSet(new EmptyItem(new Cell(i, j), Direction.NORTH)));
					numOfConveyers++;
					break;
				case 'v': // on conveyer facing up
					items.add(new ConveyerSet(new EmptyItem(new Cell(i, j), Direction.SOUTH)));
					numOfConveyers++;
					break;
				case '+': // on conveyer facing up
					items.add(new Gear(new Cell(i, j),Action.TURNRIGHT));
					numOfGears++;
					break;
				case '-': // on conveyer facing up
					items.add(new Gear(new Cell(i, j),Action.TURNLEFT));
					numOfGears++;
					break;
				case 'x': // on conveyer facing up
					items.add(new PitLocation(new Cell(i, j)));
					numOfPits++;
					break;
				case '1':
				case '2':
				case '3':
				case '4':
					EmptyItem flaggedItem = new EmptyItem(new Cell(i,j));
					flaggedItem.setFlag(true, charBoard[i][j]);
					items.add(flaggedItem);
					numOfFlags++;
					break;
				}
			}
		}

	}

public Board(int numOfConveyers, int numOfPits, int numOfFlags) {
		// !!DONT CHANGE SEED reset the this random with every new number
		totalConveyerSize = 0;
		totalPitSize = 0;
		this.numOfPits = numOfPits;
		this.numOfConveyers = numOfConveyers;
		this.numOfFlags = numOfFlags;
		// conveyers = new ArrayList<ConveyerLocation>();
		// pits = new ArrayList<PitLocation>();
		items = new ArrayList<Item>();
		GenerateItems();
		setFlags();

	}

	public void GenerateItems() {
		// generate conveyers
		for (int i = 0; i < numOfConveyers; i++) {
			items.add(new ConveyerSet(8));
			totalConveyerSize = totalConveyerSize + items.get(i).getSize();
		}
		// generate pits and ensure they dont overlap with conveyers
		for (int i = numOfConveyers; i < (numOfPits + numOfConveyers);) {
			PitLocation pit = new PitLocation();
			if (!isClashing(pit)) {
				this.items.add(pit);
				totalPitSize = totalPitSize + items.get(i).getSize();
				i++;
			}
		}
		// generate empty items for the remainder TOTAL - OCCUPIEDLOCATIONS
		int numOfRemainingLocations = (Cell.MAX_X * Cell.MAX_Y) - (totalConveyerSize + totalPitSize);

		for (int i = (totalPitSize + totalConveyerSize); i < numOfRemainingLocations;) {
			EmptyItem empty = new EmptyItem(true);
			if (!isClashing(empty)) {
				this.items.add(empty);
				i++;
			}
		}
	}

	public void setFlags() {
		// select generate locations for flags
		for (int i = 0; i < numOfFlags; i++) {
			// select an empty location by skipping conveyers and pits
			int selectedLocation = Board.rnd.nextInt(items.size()) + (totalPitSize + totalConveyerSize) - 2;
			items.get(selectedLocation).setFlag(true,i);
		}
	}

	public boolean collectFlag(Item comp) // only if the item intersects with
	{
		for (Item item : items) {
			if (item.intersects(comp) && item.getFlag() && item.getFlagNumber()==lastFlag+1) {
				lastFlag=lastFlag+1;
				item.setFlag(false,0);
				return true;
			}
		}
		return false;
	}

	// activate all
	public Item activateAll(Item comp) // only if the item intersects with
	{
		for (Item item : items) {
			if (comp.intersects(item)) {
				return item.activate(comp);
			}
		}
		return comp;
	}

	// Activate a location
	public ArrayList<Item> getItems() {
		return this.items;
	}

	// check if two items clash with any of the items currently on the system
	public boolean isClashing(Item givenLocation) {
		for (Item location : items) {

			if (location.intersects(givenLocation)) {
				return true;
			}

		}
		return false;
	}

	public int getTotalConveyerSize() {
		return this.totalConveyerSize;
	}

	public int getTotalPitSize() {
		return this.totalPitSize;
	}

	public int getNumOfConveyers() {
		return this.numOfConveyers;
	}

	public int getNumOfPits() {
		return this.numOfPits;
	}

	public int getNumOfFlags() {
		return this.numOfFlags;
	}

}