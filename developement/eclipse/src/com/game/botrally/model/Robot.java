package com.game.botrally.model;

public class Robot extends Item {

	private Program program;
	private Cell startingPosition;

	public Robot(Cell startPosition) {

		super(startPosition, Direction.NORTH);
		this.startingPosition = startPosition;
		program = new Program();
	}

	public void resetToStart() {
		this.setCell(startingPosition);
	}

	@Override // push
	public Item activate(Item position) {
		return Action.FORWARD.act(this);
	}

	public boolean addAction(Action action) {
		return program.add(action);
	}

	public void executeNext() {
		Item result = program.getNext().act(this);
		this.setCell(result.getCell());
		this.changeDirection(result.getDirection());
	}

}