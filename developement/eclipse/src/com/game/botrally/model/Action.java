package com.game.botrally.model;

public enum Action {
    FORWARD {
        @Override
        public Item act(Item position) {
            switch (position.getDirection()) {
            case NORTH:
                position.move(0, 1);
                return position;
            case SOUTH:
                position.move(0, -1);
                return position;
            case WEST:
                position.move(-1, 0);
                return position;
            case EAST:
                position.move(1, 0);
                return position;

            default:
                throw new IllegalArgumentException();
            }

        }
    },
    BACKWARD {
        @Override
        public Item act(Item position) {
            switch (position.getDirection()) {
            case NORTH:
                position.move(0, -1);
                return position;
            case SOUTH:
                position.move(0, 1);
                return position;
            case WEST:
                position.move(1, 0);
                return position;
            case EAST:
                position.move(-1, 0);
                return position;

            default:
                throw new IllegalArgumentException();
            }

        }
    },
    TURNLEFT {
        @Override
        public Item act(Item position) {

            return position.getDirection().determineDirection(TURNLEFT, position);
        }
    },
    TURNRIGHT {
        @Override
        public Item act(Item position) {

            return position.getDirection().determineDirection(TURNRIGHT, position);
        }
    },
    UTURN {
        @Override
        public Item act(Item position) {

            return position.getDirection().determineDirection(UTURN, position);
        }
    },
    WAIT {
        @Override
        public Item act(Item position) {

            return position;
        }
    };
    public abstract Item act(Item position);

    public static Action getRnd() {

        return Action.values()[Board.rnd.nextInt(Action.values().length - 1)];
    }
}