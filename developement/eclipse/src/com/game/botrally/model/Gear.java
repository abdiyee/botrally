package com.game.botrally.model;

public class Gear extends Item {
	Action directionChange;
	public Gear(Cell cell, Action directionChange) {
		super(cell);
		this.directionChange = directionChange;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Item activate(Item position) {
		// TODO Auto-generated method stub
		
		position.changeDirection(this.getDirection().determineDirection(directionChange, this).getDirection());
		return position;
	}

}
