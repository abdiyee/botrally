package com.game.botrally.model;

public enum Direction // hidden from other packages as it is only required by board
{

    NORTH {
        @Override
        public Item determineDirection(Action action, Item item) {
            switch (action) {
            case TURNLEFT:
                item = item.changeDirection(WEST);
                return item;
            case TURNRIGHT:
                item = item.changeDirection(EAST);
                return item;
            case UTURN:
                item = item.changeDirection(SOUTH);
                return item;
            default:
                throw new IllegalArgumentException();
            }
        }

    },
    EAST {
        @Override
        public Item determineDirection(Action action, Item item) {
            switch (action) {
            case TURNLEFT:
                item = item.changeDirection(NORTH);
                return item;
            case TURNRIGHT:
                item = item.changeDirection(SOUTH);
                return item;
            case UTURN:
                item = item.changeDirection(WEST);
                return item;
            default:
                throw new IllegalArgumentException();
            }
        }
    },
    WEST {
        @Override
        public Item determineDirection(Action action, Item item) {
            switch (action) {
            case TURNLEFT:
                item = item.changeDirection(SOUTH);
                return item;
            case TURNRIGHT:
                item = item.changeDirection(NORTH);
                return item;
            case UTURN:
                item = item.changeDirection(EAST);
                return item;
            default:
                throw new IllegalArgumentException();
            }
        }
    },
    SOUTH {
        @Override
        public Item determineDirection(Action action, Item item) {
            switch (action) {
            case TURNLEFT:
                item = item.changeDirection(EAST);
                return item;
            case TURNRIGHT:
                item = item.changeDirection(WEST);
                return item;
            case UTURN:
                item = item.changeDirection(NORTH);
                return item;
            default:
                throw new IllegalArgumentException();
            }
        }
    };
    abstract public Item determineDirection(Action action, Item item);
    public static Direction getRnd()
    {
     
      return  Direction.values()[Board.rnd.nextInt(Direction.values().length - 1)];
    }
}