package com.game.botrally;


import java.io.IOException;

//import com.game.botrally.BoardController;
//import com.game.botrally.controller.*;

//import com.game.botrally.controller.BoardController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class MenuController {
	@FXML
	public void playPressed() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("SelectFileScene.fxml"));
		final SelectFileController controller = new SelectFileController();
		loader.setController(controller);
		try {
			final Parent parent = (Parent) loader.load();

			final Stage selectStage = new Stage();
			selectStage.initStyle(StageStyle.UNDECORATED);
			selectStage.initModality(Modality.APPLICATION_MODAL);
			selectStage.setScene(new Scene(parent, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));
			selectStage.showAndWait();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	public void exitPressed() {
		System.exit(0);
	}
}
