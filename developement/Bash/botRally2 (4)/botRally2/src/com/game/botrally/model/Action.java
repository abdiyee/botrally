package com.game.botrally.model;

enum Action {
    FORWARD {
        @Override
        public Position act(Position position) {
            switch (position.getDirection()) {
            case NORTH:
                position.move(0, 1);
                return position;
            case SOUTH:
                position.move(0, -1);
                return position;
            case WEST:
                position.move(-1, 0);
                return position;
            case EAST:
                position.move(1, 0);
                return position;

            default:
                throw new IllegalArgumentException();
            }

        }
    },
    BACKWARD {
        @Override
        public Position act(Position position) {
            switch (position.getDirection()) {
            case NORTH:
                position.move(0, -1);
                return position;
            case SOUTH:
                position.move(0, 1);
                return position;
            case WEST:
                position.move(1, 0);
                return position;
            case EAST:
                position.move(-1, 0);
                return position;

            default:
                throw new IllegalArgumentException();
            }

        }
    },
    TURNLEFT {
        @Override
        public Position act(Position position) {

            return new Position(position.getCell(), 
                    position.getDirection().changeDirection(TURNLEFT));
        }
    },
    TURNRIGHT {
        @Override
        public Position act(Position position) {

            return new Position(position.getCell(), 
                    position.getDirection().changeDirection(TURNRIGHT));
        }
    },
    UTURN {
        @Override
        public Position act(Position position) {

            return new Position(position.getCell(), 
                    position.getDirection().changeDirection(UTURN));
        }
    },
    WAIT {
        @Override
        public Position act(Position position) {

            return position;
        }
    };
    public abstract Position act(Position position);
}