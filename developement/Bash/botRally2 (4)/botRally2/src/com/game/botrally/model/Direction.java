package com.game.botrally.model;

enum Direction // hidden from other packages as it is only required by board
{

    NORTH {
        @Override
        public Direction changeDirection(Action action) {
            switch (action) {
            case TURNLEFT:
                return WEST;
            case TURNRIGHT:
                return EAST;
            case UTURN:
                return SOUTH;
            default:
                throw new IllegalArgumentException();
            }
        }

    },
    EAST {
        @Override
        public Direction changeDirection(Action action) {
            switch (action) {
            case TURNLEFT:
                return NORTH;
            case TURNRIGHT:
                return SOUTH;
            case UTURN:
                return WEST;
            default:
                throw new IllegalArgumentException();
            }
        }
    },
    WEST {
        @Override
        public Direction changeDirection(Action action) {
            switch (action) {
            case TURNLEFT:
                return SOUTH;
            case TURNRIGHT:
                return NORTH;
            case UTURN:
                return EAST;
            default:
                throw new IllegalArgumentException();
            }
        }
    },
    SOUTH {
        @Override
        public Direction changeDirection(Action action) {
            switch (action) {
            case TURNLEFT:
                return WEST;
            case TURNRIGHT:
                return EAST;
            case UTURN:
                return NORTH;
            default:
                throw new IllegalArgumentException();
            }
        }
    };
    abstract public Direction changeDirection(Action action);
}