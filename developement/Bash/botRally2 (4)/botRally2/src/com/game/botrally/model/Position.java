package com.game.botrally.model;

class Position {
    private Cell cell;
    private Direction direction;

    public Position(Cell cell, Direction direction) {
        this.cell = cell;
        this.direction = direction;
    }

    public void move(int x, int y) {
        this.cell.setX(this.cell.getX() + x);
        this.cell.setY(this.cell.getY() + y);

    }

    public Position direction(Direction direction) {
        this.direction = direction;
        return this;
    }

    public Cell getCell() {
        return this.cell;
    }

    public Direction getDirection() {
        return this.direction;
    }

}