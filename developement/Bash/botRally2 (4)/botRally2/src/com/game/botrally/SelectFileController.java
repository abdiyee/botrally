package com.game.botrally;

import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;


public class SelectFileController {
	@FXML private Button btnSelectBoardFile;
	@FXML private Button btnSelectProgram;
	@FXML private File boardFile;
	@FXML private File programFile;
	
	public void initialize() {
		
		
	}
	
	@FXML
	public void selectBoardFilePressed() {
		
		boardFile = selectFile();
		if (boardFile != null && boardFile.getName().endsWith(".brd")) { //if "null" statement to avoid an error of checking name of file that doesn't exist
			
			System.out.println("This is a board File!");
			
		}
		else
		{
			System.out.println("Wrong file Type! Please select a board (.brd) file!");
		}
		
	}
	
	@FXML
	public void programPressed() {
		
		programFile = selectFile();
		if (programFile != null && programFile.getName().endsWith(".prg")) {
			
			System.out.println("This is a program File!");
			
		}
		else
		{
			System.out.println("Wrong file Type! Please select a program (.prg) file!");
		}
		
	}
	
	@FXML
	public void startPressed() {
		if (boardFile == null || programFile == null) {
			
			System.out.println("Please Select a Program and Board File!");
			
			return;
			
		}
		
		final FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("BoardScene.fxml"));
		final BoardController controller = new BoardController();
		loader.setController(controller);
		controller.setBoardFile(boardFile); //Transfer the board file selected in this class to the BoardController Class
		controller.setProgramFile(programFile); //Transfer the board file selected in this class to the BoardController Class
		
		try {
			final Parent parent = (Parent) loader.load();

			final Stage boardStage = new Stage();
			boardStage.initStyle(StageStyle.UNDECORATED);
			boardStage.initModality(Modality.APPLICATION_MODAL);
			boardStage.setScene(new Scene(parent, Region.USE_PREF_SIZE, Region.USE_PREF_SIZE));
			boardStage.showAndWait();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
	}
	
	
	@FXML
	private File selectFile() {
		File select;
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File("./Assets"));
		select = fileChooser.showOpenDialog(null);
		return select;
		
		
	}
	
	public File getBoard() {
		
		return boardFile;
	}
	
	@FXML
	public void exitPressed() {
		btnSelectBoardFile.getScene().getWindow().hide();
	}
	
	
}
