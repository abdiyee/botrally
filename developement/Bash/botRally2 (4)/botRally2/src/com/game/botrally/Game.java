package com.game.botrally;
//import com.game.botrally.model.*;



import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Game extends Application{

	public void start(Stage primaryStage) {
		try {
			final FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("MenuScene.fxml"));
			loader.setController(new MenuController());
			final Parent root = loader.load();
			
			primaryStage.initStyle(StageStyle.UNDECORATED);
			
			
			
			final Scene scene = new Scene(root, Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
			primaryStage.setTitle("BotRally");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
} 