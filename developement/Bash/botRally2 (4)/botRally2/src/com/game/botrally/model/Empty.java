package com.game.botrally.model;

public class Empty extends Location  // hidden from other packages as it is only required by board
{

    public Empty(Cell cell)
    {
        super(cell);
    }
    
    @Override // does nothing
    public Position activate(Position position) {
        return position; // because this location does nothing
    }


}