package com.game.botrally.model;
abstract class Location
{
    Cell cell;

    public Location(Cell cell) {
        this.cell = cell;
    }

    public Cell getCell() {
        return this.cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }
      
    abstract public Position activate(Position position);

   

    
}