package com.game.botrally.model;

public class Player 
{
    public static int numOfPlayers = 0;
    private String name;
    private int points;
    private Robot robot;
    private int code;

    public Player(String name) {
       
        this.name = name;
        this.points = 0;
        this.robot = new Robot(numOfPlayers); // place on x based on player number
        
        this.code = Player.numOfPlayers;
        Player.numOfPlayers++;
    }


    public String getName() {
        return this.name;
    }

    public int getPoints() {
        return this.points;
    }

    public void addPoints(int points) {
        this.points = this.points + points;
    }

    public Robot getRobot() {
        return this.robot;
    }

    public int getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        return "{" +
            "name   = '" + name + "'" +
            "points = '" + points + "'" +
            "robot  = '" + robot + "'" +
            "code   = '" + code + "'" +
            "}";
    }





}