package com.game.botrally;

import javafx.stage.FileChooser;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;


public class BoardController {
	
	private ArrayList<String> lineContent = new ArrayList<String>();
	private char[][] board; 
	private int ysize;
	private int xsize;
	@FXML private File boardFile;
	@FXML private File programFile;
	@FXML private Label lblStart;
@FXML
public void initialize() {
	
	
	
	fillArray();

}
	
public void setBoardFile(File board) {
	
	boardFile = board;
}

public void setProgramFile(File program) {
	
	programFile = program;
}
	
@FXML
public void exitPressed() {
	
	
	lblStart.getScene().getWindow().hide();
}



public void fillArray() {
	
	BufferedReader reader = null;
	
	if (boardFile == null) {
			
			System.out.println("No File has been selected!");
			return;
	}
	
	
    try {
        reader = new BufferedReader(new FileReader(boardFile));
        String line = reader.readLine();
        
        if (line.contentEquals("format 1")) {
        	System.out.println("The format (format 1) is Correct!");	
        }
        else {
        	System.out.println("The format is incorrect! File must start with format 1");
        }
        
        line = reader.readLine();
        while (line != null) {
            
        	lineContent.add(line);
            line = reader.readLine();
            
        }
        
        if(lineContent.size() < 1) {
        	System.out.println("There is nothing on the Board!");
        	reader.close();
        	return;
        	
        }
        
        ysize = lineContent.size();
        xsize = lineContent.get(0).length();
        board = new char[ysize][xsize];
        String tmpline = lineContent.get(0);
        
        for(String s : lineContent) {
        	
        	if (s.length() == tmpline.length()) {
        		
        		tmpline = s;
        	}
        	else
        	{
        		
        		System.out.println("The Lengths of the rows are not the same!");
        		reader.close();
        		return;
        	}
        	
        }
       
        for (int i = 0; i < lineContent.size(); i++) {
        	String s = lineContent.get(i);
        	int count = 0;
        	char[] a=s.toCharArray();
        	for(char c:a){
        		board[i][count] = c;
        		System.out.print(board[i][count]);
        		count++;
        	}
        	System.out.println();
		}
        
        
        
        
        System.out.println("This is a (y)" + ysize + " by (x)" + xsize + " Board!");
        reader.close();
    } catch (IOException e) {
    	System.out.println("The File Does not Exist!");
    }
   
    	checkSymbol();
    
    }
@FXML
private void selectFile() {
	
	FileChooser fileChooser = new FileChooser();
	fileChooser.setInitialDirectory(new File("./Assets"));
	boardFile = fileChooser.showOpenDialog(null);
	
	
}

@FXML
private void checkSymbol() {

	for (int i = 0; i < ysize; i++) {
		for (int j = 0; j < xsize; j++) {
			
			switch (board[i][j]) {
				case '.':
					System.out.println("This is an empty tile");
					break;
				case 'A':
					System.out.println("This is starting position 1");
					break;
				case 'B':
					System.out.println("This is starting position 2");
					break;
				case 'C':
					System.out.println("This is starting position 3");
					break;
				case 'D':
					System.out.println("This is starting position 4");
					break;
				case '+':
					System.out.println("This is Clock-wise gear");
					break;
				case '-':
					System.out.println("This is counter Clock-wise gear");
					break;
				case '1':
					System.out.println("This is the 1st class");
					break;
				case '2':
					System.out.println("This is the 2st class");
					break;
				case '3':
					System.out.println("This is the 3st class");
					break;
				case '4':
					System.out.println("This is the 1st class");
					break;
				case 'x':
					System.out.println("This is a pit");
					break;
				case 'v':
					System.out.println("This is the down straight conveyor belt");
					break;
				case '>':
					System.out.println("This is the right stright conveyor belt");
					break;
				case '<':
					System.out.println("This is the left stright conveyor belt");
					break;
				case '^':
					System.out.println("This is the up stright conveyor belt");
					break;
			
			}
			
		}
	}

}
}


